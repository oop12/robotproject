
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(3, 4, 'R', map);
        Bomb bomb = new Bomb(7, 9);
        map.setRobot(robot);
        map.setBomb(bomb);
        while (true) {
            map.showmap();
            char direction = inputDirection(kb);
            if (direction == 'q') {
                System.out.println("Bye!!!");
                break;
            }
            robot.walk(direction);
        }

    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        return str.charAt(0);
    }

}
