/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private TableMap map;

    public Robot(int x, int y, char symbol, TableMap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                walkN();
                break;

            case 'S':
            case 's':
                walkS();
                break;

            case 'E':
            case 'd':
                walkE();
                break;

            case 'W':
            case 'a':
                walkW();
                break;
        }
        checkBomb();
        return true;
    }

    private void walkW() {
        if (map.inMap(x - 1, y)) {
            x = x - 1;
        } else {
            printDontMove();
        }
    }

    private void walkE() {
        if (map.inMap(x + 1, y)) {
            x = x + 1;
        } else {
            printDontMove();
        }
    }

    private void walkS() {
        if (map.inMap(x, y + 1)) {
            y = y + 1;
        } else {
            printDontMove();
        }
    }

    private void walkN() {
        if (map.inMap(x, y - 1)) {
            y = y - 1;
        } else {
            printDontMove();
        }
    }

    private void checkBomb() {
        if (map.isBomb(x, y)) {
            System.out.println("Bomb found(" + x + "," + y + ")!!!");
        }
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }

    public void printDontMove() {
        System.out.println("I can't move!!!");
    }

}
